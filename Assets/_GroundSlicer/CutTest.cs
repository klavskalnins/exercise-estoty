using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutTest : MonoBehaviour
{
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 1000f))
            {
                Debug.Log("GO: " + hit.triangleIndex);
                DeleteTri(hit.triangleIndex);
            }
        }
    }

    public void DeleteTri(int index)
    {
        Destroy(this.gameObject.GetComponent<MeshCollider>());
        Mesh mesh = transform.GetComponent<MeshFilter>().mesh;
        int[] oldTri = mesh.triangles;
        int[] newTri = new int[mesh.triangles.Length - 3];

        int i = 0;
        int j = 0;
        while (j < mesh.triangles.Length)
        {
            if (j != index * 3)
            {
                newTri[i++] = oldTri[j++];
                newTri[i++] = oldTri[j++];
                newTri[i++] = oldTri[j++];
            }
            else
            {
                j += 3;
            }

            transform.GetComponent<MeshFilter>().mesh.triangles = newTri;
            //gameObject.AddComponent<MeshCollider>();

        }
        MeshGenerator.instance.RecalculateCollider();
        // gameObject.AddComponent<MeshCollider>();
        // mesh.RecalculateBounds();
        // MeshCollider meshCollider = gameObject.GetComponent<MeshCollider>();
        // meshCollider.sharedMesh = mesh;

        //MeshGenerator.instance.UpdateMesh();
    }
}
